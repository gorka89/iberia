	
/**
 * Content as string, another options is to store at s3 and read the bytes
 */
package com.bbt.bcubemagazine;

import android.util.Log;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

    public class Application extends android.app.Application {
        @Override
        public void onCreate(){
            // i cant call twice :(\n" +
            // _default app created in dev account javier.arnaiz at parse.com\n" +
            Parse.initialize(Application.this, "GFOGYhM9AdQ7j5sbXLhCblubQHvymMYBb8IQ5VVh", "8EdCisdo6vh5fr9DQkPibbzoH7tKcPdv4VSLZwDb");
            PushService.setDefaultPushCallback(Application.this, Iberia.class);
            Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
            // we need to add blocked operations in other thread\n" +
            new Thread(new Runnable(){
                @Override
                public void run() {
                    // get device token with retries\n" +
                    String deviceToken = getDeviceToken(10);
                    try {
                        // get request\n" +
                        HttpClient httpClient = new DefaultHttpClient();
                        String endpointPush = "http://dashboard.upplication.com/appsCommunication/rest/app/push";
                        HttpPost post =
                                new HttpPost(endpointPush);
                        post.setHeader("content-type", "application/json");
                        post.setHeader("childupp", "4134");
                        post.setHeader("User-Agent", "Mozilla/5.0 (Linux; U; Android 2.2; en-gb; Nexus One Build/FRF50) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
                        post.setEntity(new StringEntity("{\"device\": \"" + deviceToken + "\"}"));
                        httpClient.execute(post);
                    } catch (Exception  e) {
                        Log.e("UPPLICATION", "Error al intentar comunicarme con la api rest", e);
                    }
                }
                // start it!\n" +
            }).start();
        }

        private String getDeviceToken(int retry){
            try {
                String result = ParseInstallation.getCurrentInstallation().getString("deviceToken");
                if (result == null && retry > 0){
                    Thread.sleep(1000);
                    return getDeviceToken(--retry);
                }
                else {
                    if (retry <= 0){
                        Log.e("UPPLICATION", "NO hemos podido recuperar el devicetoken en los intentos establecidos");
                    }
                    return result;
                }
            }
            catch (Exception e){
                Log.e("UPPLICATION", "Error al intentar recuperar el devicetoken", e);
                return null;
            }
        }
    }






